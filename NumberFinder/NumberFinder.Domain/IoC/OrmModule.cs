﻿using System.Data.Entity;
using Autofac;
using NumberFinder.Domain.Generator;

namespace NumberFinder.Domain.IoC
{
  public class OrmModule : Module
  {
    protected override void Load(ContainerBuilder builder)
    {
      builder.RegisterAssemblyTypes(ThisAssembly)
             .AssignableTo<DbContext>()
             .AsSelf()
             .AsImplementedInterfaces()
             .As<DbContext>()
             .InstancePerLifetimeScope();

      builder.RegisterType<DataGenerator>().As<IDataGenerator>();
    }
  }
}
