﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberFinder.Domain.Entities
{
  public class NumberFinderImplementation
  {
    public int Id { get; set; }
    public string Name { get; set; }

    public virtual List<NumberFinderRun> Runs { get;set; }
  }
}
