﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberFinder.Domain.Entities
{
  public class NumberFinderRun
  {
    public int Id { get;set; }

    public TimeSpan Duration { get;set; }

    public virtual NumberSet NumberSet { get; set; }
    public virtual NumberFinderImplementation NumberFinderImplementation { get; set; }
  }
}
