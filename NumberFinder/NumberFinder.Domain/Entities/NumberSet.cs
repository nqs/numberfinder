﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace NumberFinder.Domain.Entities
{
  public class NumberSet
  {
    public int Id { get; set; }
    public int Max { get; set; }
    public int Min { get; set; }
    public int MissingNumber { get; set; }

    public virtual List<NumberFinderRun> Runs { get;set; }

  }
}
