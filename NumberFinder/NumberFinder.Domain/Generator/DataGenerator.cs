﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumberFinder.Domain.Context;
using NumberFinder.Domain.Entities;

namespace NumberFinder.Domain.Generator
{
  public class DataGenerator : IDataGenerator
  {
    private NumberFinderContext _dbContext;

    public DataGenerator(NumberFinderContext dbContext)
    {
      _dbContext = dbContext;
    }

    public void Generate()
    {
      if (!_dbContext.NumberSets.Any())
      {
        var random = new Random();
        for (int x = 1; x <= 10; x++)
        {
          var set = new NumberSet();
          set.Max = x * 10 + 1;
          set.Min = x * 5 + 1;
          set.MissingNumber = set.Min + ((set.Max - set.Min) / 2);

          _dbContext.NumberSets.Add(set);
        }
        _dbContext.SaveChanges();
        var implementation = _dbContext.NumberFinderImplementations.First();
        foreach (var set in _dbContext.NumberSets)
        {
          for(int x =0; x < 10; x++)
          {
            var run = new NumberFinderRun();
            run.NumberSet = set;
            run.Duration = TimeSpan.FromMilliseconds(10 * x);
            run.NumberFinderImplementation = implementation;
            _dbContext.NumberFinderRuns.Add(run);
          }
        }
        _dbContext.SaveChanges();

      }
    }
  }
}
