﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberFinder.Domain.Generator
{
  public interface IDataGenerator
  {
    void Generate();
  }
}
