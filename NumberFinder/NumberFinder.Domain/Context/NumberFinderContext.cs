﻿#region usings

using System.Data.Entity;
using System.Reflection;
using NumberFinder.Domain.Entities;

#endregion

namespace NumberFinder.Domain.Context
{
  public class NumberFinderContext : DbContext
  {
   /* static NumberFinderContext()
    {
      Database.SetInitializer<NumberFinderContext>(null);
    }*/

    public NumberFinderContext()
        : base("Name=NumberFinderContext")
    {
    }

    public DbSet<NumberSet> NumberSets { get; set; }

    public DbSet<NumberFinderImplementation> NumberFinderImplementations { get; set; }
    public DbSet<NumberFinderRun> NumberFinderRuns { get; set; }
  }
}