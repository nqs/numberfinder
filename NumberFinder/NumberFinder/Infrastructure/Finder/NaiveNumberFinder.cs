﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberFinder.Infrastructure.Finder
{
    public class NaiveNumberFinder : INumberFinder
    {
        public int FindMissingNumber(int[] sequence)
        {
           // return 0;

            var s = sequence.OrderBy(num => num).ToArray() ;
            var m = s[0];

            for (int i = 0; i < s.Count(); i++)
            {

                if (m == s[i])
                { m++; }
                else { return m; }

            }

            throw new Exception();

        }
    }
}
