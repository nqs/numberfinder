﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NumberFinder.Infrastructure.Configuration;


namespace NumberFinder.Infrastructure.IO
{
    public class File3NumberProvider: INumberProvider
    {
        protected IConfigurationProvider _config;
        public File3NumberProvider(IConfigurationProvider config)
        {
            _config = config;
        }

        public int[] GetNumbers()
        {
            var text = File.ReadAllText(_config.InputFileLocation());
            return text.Split(',').Select(line => Convert.ToInt32(line)).ToArray();
        }
  
    }
}
