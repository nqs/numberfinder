﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberFinder.Infrastructure.IO
{
  public class RangeNumberProvider : INumberProvider
  {
    private int _min;
    private int _max;
    private int _missing;

    public RangeNumberProvider(int min, int max, int missing)
    {
      _min = min;
      _max = max;
      _missing = missing;
    }

    public int[] GetNumbers()
    {
      var result = new List<int>();
      for(int x = _min; x < _max; x++)
      {
        if (x != _missing)
          result.Add(x);
      }
      return result.ToArray();
    }
  }
}
