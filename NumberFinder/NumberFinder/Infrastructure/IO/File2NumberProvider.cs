﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NumberFinder.Infrastructure.Configuration;

namespace NumberFinder.Infrastructure.IO
{
    public class File2NumberProvider: INumberProvider 

    {
        protected IConfigurationProvider _config;
        public File2NumberProvider(IConfigurationProvider config)
        {
            _config = config;
        }


        public int[] GetNumbers()
        {
            var lines = File.ReadAllLines(_config.InputFileLocation()); 

            var nums = lines[0].Split(',').Select(line => Convert.ToInt32(line)).ToArray();
            
            return nums;
         
        }
    }
}
