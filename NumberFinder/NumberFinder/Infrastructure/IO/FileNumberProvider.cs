﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using NumberFinder.Infrastructure.Configuration;

namespace NumberFinder.Infrastructure.IO
{
    public class FileNumberProvider : INumberProvider
    {
        protected IConfigurationProvider _config;
        public FileNumberProvider(IConfigurationProvider config)
        {
            _config = config;
        }

        public int[] GetNumbers()
        {
            var lines = File.ReadAllLines(_config.InputFileLocation());
            var minLine = lines[0];
            var min = Convert.ToInt32(minLine.Split(':')[1]);
            var maxLine = lines[1];
            var max = Convert.ToInt32(maxLine.Split(':')[1]);

            var nums = lines.Skip(2).Select(line => Convert.ToInt32(line.Trim())).ToArray();
            if (nums.Min() < min)
                throw new Exception("Invalid min");
            else if (nums.Max() > max)
                throw new Exception("Invalid max");
            else
                return nums;

            //return File.ReadAllLines(_config.InputFileLocation()).Select(line => Convert.ToInt32(line.Trim())).ToArray();
        }
    }
}
