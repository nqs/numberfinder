﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumberFinder.Infrastructure.IO;
using NumberFinder.Infrastructure.Finder;
using NumberFinder.Domain.Context;
using NumberFinder.Domain.Generator;
using NumberFinder.Domain.Entities;

namespace NumberFinder
{
    public class DatabaseSequenceProcessor : ISequenceProcessor
    {
        protected NumberFinderContext _context;
        protected INumberFinder _finder;
        protected IDataGenerator _generator;

        public DatabaseSequenceProcessor(NumberFinderContext context, INumberFinder finder, IDataGenerator generator)
        {
            _context = context;
            _finder = finder;
            _generator = generator;
        }

        public int GetMissingNumber()
        {
            //Put some data in the tables, create them if they're not there
            _generator.Generate();

            //Pull all the sets
            var sets = from s in _context.NumberSets
                       select s;

            //Select the first one
            var firstSet = sets.First();

            var stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();
            //TODO:  Replace with Indexed Autofac registration
            var provider = new RangeNumberProvider(firstSet.Min, firstSet.Max, firstSet.MissingNumber);
            stopwatch.Stop();

            var runtime = stopwatch.Elapsed;

            var name = _finder.GetType().Name;

       
            var imp = new NumberFinderImplementation();
            imp.Name = name;
            _context.NumberFinderImplementations.Add(imp);

            var run = new NumberFinderRun();
            run.NumberFinderImplementation = imp;
            run.NumberSet = firstSet;
            run.Duration = runtime;
            _context.NumberFinderRuns.Add(run);

            
            _context.SaveChanges();


            //return the result
            return _finder.FindMissingNumber(provider.GetNumbers());
        }
    }
}
