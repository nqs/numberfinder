﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using NumberFinder.Infrastructure.Finder;
using NumberFinder.Infrastructure.IO;
namespace NumberFinder.Tests
{
  [TestClass]
  public class SequenceProcessorTests
  {
    [TestMethod]
    public void GetMissingNumberTest()
    {
      //simple array to use
      var a = new int[2] { 1, 3 };

      //the NumberProvider returns the array above
      var numProvider = Mock.Of<INumberProvider>(p => p.GetNumbers() == a);

      //the NumberFinder, when given the array above, returns 2
      var numFinder = Mock.Of<INumberFinder>(f => f.FindMissingNumber(a) == 2);
      
      //Does the processor give us back what we expect?
      var proc = new SequenceProcessor(numProvider, numFinder);

      Assert.AreEqual(proc.GetMissingNumber(), 2);
    }
  }
}
