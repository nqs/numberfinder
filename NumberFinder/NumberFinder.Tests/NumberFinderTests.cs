﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberFinder.Infrastructure.Finder;

namespace NumberFinder.Tests
{
    [TestClass]
    public class NumberFinderTests
    {
        [TestMethod]
        public void FindMissingNumberTest()
        {
            var sequence = new int[] { 1, 2, 3, 4, 5, 7, 8, 9, 10 };
            var answer = 6;

            var finder = new NaiveNumberFinder();

            var missing = finder.FindMissingNumber(sequence);

            Assert.AreEqual(answer, missing);
        }
    }
}
